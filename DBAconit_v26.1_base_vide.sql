# Export de la base de donnée "aconit_loc" de la machine 127.0.0.1
# MySQL mysqlnd 8.1.8
# DBAconit V26.2
# Date : 2023-04-20  16:41


#============================================================== Acquisitions ===
DROP TABLE IF EXISTS Acquisitions;
CREATE TABLE `Acquisitions` (
  `idacquisition` tinyint(4) NOT NULL AUTO_INCREMENT,
  `acquisition` char(30) DEFAULT NULL,
  `acquisition_en` char(30) DEFAULT NULL,
  PRIMARY KEY (`idacquisition`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO Acquisitions (idacquisition, acquisition, acquisition_en) VALUES
  ('1','(non renseigné)','(not documented)'),
  ('2','don','gift'),
  ('3','dépôt','deposit'),
  ('4','achat','purchase'),
  ('5','construction en interne','internal construction'),
  ('6','prêt','loan');


#================================================================= Batiments ===
DROP TABLE IF EXISTS Batiments;
CREATE TABLE `Batiments` (
  `idbatiment` smallint(6) NOT NULL AUTO_INCREMENT,
  `idetablissement` smallint(6) DEFAULT NULL,
  `cotebatiment` char(6) DEFAULT NULL,
  `batiment` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`idbatiment`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#================================================================= Codeprogs ===
DROP TABLE IF EXISTS Codeprogs;
CREATE TABLE `Codeprogs` (
  `idcodeprog` tinyint(4) NOT NULL AUTO_INCREMENT,
  `codeprog` char(20) DEFAULT NULL,
  `codeprog_en` char(20) DEFAULT NULL,
  PRIMARY KEY (`idcodeprog`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO Codeprogs (idcodeprog, codeprog, codeprog_en) VALUES
  ('1','(non renseigné)','(not documented)'),
  ('2','code source','source code'),
  ('3','module objet','object module'),
  ('4','code binaire','binary code'),
  ('5','progiciel','software package'),
  ('6','script (interprété','script (interpreted)');


#================================================================== Col_Mate ===
DROP TABLE IF EXISTS Col_Mate;
CREATE TABLE `Col_Mate` (
  `idcollection` mediumint(4) NOT NULL DEFAULT '0',
  `idmateriau` mediumint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idcollection`,`idmateriau`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#=================================================================== Col_Med ===
DROP TABLE IF EXISTS Col_Med;
CREATE TABLE `Col_Med` (
  `idcollection` mediumint(9) NOT NULL DEFAULT '0',
  `idmedia` mediumint(9) NOT NULL DEFAULT '0',
  `ordre_media` smallint(6) DEFAULT NULL,
  `mediacle` enum('oui','non') NOT NULL DEFAULT 'non',
  PRIMARY KEY (`idcollection`,`idmedia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#=================================================================== Col_Mot ===
DROP TABLE IF EXISTS Col_Mot;
CREATE TABLE `Col_Mot` (
  `idcollection` mediumint(4) NOT NULL DEFAULT '0',
  `idmotcle` mediumint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idcollection`,`idmotcle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#================================================================== Col_Mouv ===
DROP TABLE IF EXISTS Col_Mouv;
CREATE TABLE `Col_Mouv` (
  `idcollection` mediumint(9) NOT NULL DEFAULT '0',
  `idmouvement` smallint(6) NOT NULL DEFAULT '0',
  `commouv` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idcollection`,`idmouvement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#=================================================================== Col_Org ===
DROP TABLE IF EXISTS Col_Org;
CREATE TABLE `Col_Org` (
  `idcollection` mediumint(9) NOT NULL DEFAULT '0',
  `idorganisme` mediumint(9) NOT NULL DEFAULT '0',
  `idrelation` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcollection`,`idorganisme`,`idrelation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#=================================================================== Col_Per ===
DROP TABLE IF EXISTS Col_Per;
CREATE TABLE `Col_Per` (
  `idcollection` mediumint(9) NOT NULL DEFAULT '0',
  `idpersonne` mediumint(9) NOT NULL DEFAULT '0',
  `idrelation` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcollection`,`idpersonne`,`idrelation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#=============================================================== Collections ===
DROP TABLE IF EXISTS Collections;
CREATE TABLE `Collections` (
  `idcollection` mediumint(9) NOT NULL AUTO_INCREMENT,
  `type` char(12) DEFAULT NULL,
  `nrinv` varchar(12) DEFAULT NULL,
  `nrancien` varchar(80) DEFAULT NULL,
  `date1` date DEFAULT NULL,
  `date2` date DEFAULT NULL,
  `idperiode` tinyint(4) NOT NULL DEFAULT '1',
  `moyens` varchar(50) DEFAULT NULL,
  `acquis` varchar(20) DEFAULT NULL,
  `idacquisition` tinyint(4) NOT NULL DEFAULT '1',
  `feuilledon` tinyint(4) NOT NULL DEFAULT '0',
  `prix` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `valeurest` mediumint(9) DEFAULT NULL,
  `valeurorg` varchar(50) DEFAULT NULL,
  `utillieu` varchar(50) DEFAULT NULL,
  `utilpays` varchar(20) DEFAULT NULL,
  `inscri` text,
  `constatetat` text,
  `modif` text,
  `description` text,
  `descutilisation` text,
  `couleur` varchar(20) DEFAULT NULL,
  `dimlong` float DEFAULT NULL,
  `dimlarg` float DEFAULT NULL,
  `dimhaut` float DEFAULT NULL,
  `dimdiam` float DEFAULT NULL,
  `dimpoids` float DEFAULT NULL,
  `comdim` varchar(50) DEFAULT NULL,
  `idetablissement` smallint(6) NOT NULL DEFAULT '1',
  `coterange` varchar(25) DEFAULT NULL,
  `lieurange` varchar(50) DEFAULT NULL,
  `daterange` date DEFAULT NULL,
  `nouvrange` varchar(50) DEFAULT NULL,
  `museevirtuel` char(7) DEFAULT NULL,
  `notes` text,
  `nomrap` varchar(30) DEFAULT NULL,
  `nomrapcreat` varchar(30) DEFAULT NULL,
  `iddomaine` tinyint(4) NOT NULL DEFAULT '1',
  `idetat` tinyint(4) NOT NULL DEFAULT '1',
  `idproduction` tinyint(4) NOT NULL DEFAULT '1',
  `datecreat` date DEFAULT NULL,
  `datemod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `datemodrap` date DEFAULT NULL,
  `datetransfert` date DEFAULT NULL,
  `idmachine` mediumint(9) DEFAULT NULL,
  `iddocument` mediumint(9) DEFAULT NULL,
  `idlogiciel` mediumint(9) DEFAULT NULL,
  `idetatfiche` tinyint(4) NOT NULL DEFAULT '1',
  `idtrecol` tinyint(4) NOT NULL DEFAULT '1',
  `idtnposition` tinyint(4) NOT NULL DEFAULT '1',
  `m1` tinyint(4) NOT NULL DEFAULT '0',
  `m2` tinyint(4) NOT NULL DEFAULT '0',
  `m3` tinyint(4) NOT NULL DEFAULT '0',
  `m4` tinyint(4) NOT NULL DEFAULT '0',
  `m5` tinyint(4) NOT NULL DEFAULT '0',
  `m6` tinyint(4) NOT NULL DEFAULT '0',
  `m7` tinyint(4) NOT NULL DEFAULT '0',
  `m8` tinyint(4) NOT NULL DEFAULT '0',
  `m9` tinyint(4) NOT NULL DEFAULT '0',
  `m10` tinyint(4) NOT NULL DEFAULT '0',
  `tampond` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `res2c` varchar(100) DEFAULT NULL,
  `res3c` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idcollection`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



#================================================================= Compteurs ===
DROP TABLE IF EXISTS Compteurs;
CREATE TABLE `Compteurs` (
  `idcompteur` tinyint(4) NOT NULL AUTO_INCREMENT,
  `compteur` char(50) DEFAULT NULL,
  `compte` int(11) NOT NULL DEFAULT '0',
  KEY `idcompteur` (`idcompteur`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO Compteurs (idcompteur, compteur, compte) VALUES
  ('1','Remise à zéro le 9/1/2020','0'),
  ('2','nbr_acces_inscrits','0'),
  ('3','nbr_acces_visiteurs','0'),
  ('4','nbr_lectures_inscrits','0'),
  ('5','nbr_lectures_visiteurs','0'),
  ('6','nbr_modifs','0'),
  ('7','nbr_conflits','0');


#============================================================== Designations ===
DROP TABLE IF EXISTS Designations;
CREATE TABLE `Designations` (
  `iddesignation` mediumint(9) NOT NULL AUTO_INCREMENT,
  `designation` char(30) DEFAULT NULL,
  `designation_en` char(30) DEFAULT NULL,
  PRIMARY KEY (`iddesignation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#================================================================= Documents ===
DROP TABLE IF EXISTS Documents;
CREATE TABLE `Documents` (
  `iddocument` mediumint(9) NOT NULL AUTO_INCREMENT,
  `titredoc` varchar(100) DEFAULT NULL,
  `titredoc_fr` varchar(100) DEFAULT NULL,
  `collection` varchar(50) DEFAULT NULL,
  `refediteur` varchar(20) DEFAULT NULL,
  `isbn` varchar(20) DEFAULT NULL,
  `issn` varchar(20) DEFAULT NULL,
  `nrbrevet` varchar(20) DEFAULT NULL,
  `production` varchar(100) DEFAULT NULL,
  `technique` varchar(100) DEFAULT NULL,
  `villepays` varchar(50) DEFAULT NULL,
  `nbrepage` smallint(6) DEFAULT NULL,
  `format` varchar(20) DEFAULT NULL,
  `nrpage` varchar(20) DEFAULT NULL,
  `idtypedoc` tinyint(4) NOT NULL DEFAULT '1',
  `idtypeillustr` tinyint(4) NOT NULL DEFAULT '1',
  `idlangue` tinyint(4) NOT NULL DEFAULT '1',
  `res1d` varchar(100) DEFAULT NULL,
  `res2d` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`iddocument`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#================================================================== Domaines ===
DROP TABLE IF EXISTS Domaines;
CREATE TABLE `Domaines` (
  `iddomaine` tinyint(4) NOT NULL AUTO_INCREMENT,
  `domaine` char(100)  DEFAULT NULL,
  `domaine_en` char(100) DEFAULT NULL,
  PRIMARY KEY (`iddomaine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#============================================================ Etablissements ===
DROP TABLE IF EXISTS Etablissements;
CREATE TABLE `Etablissements` (
  `idetablissement` smallint(6) NOT NULL AUTO_INCREMENT,
  `prefinv` char(20) DEFAULT NULL,
  `etablissement` varchar(100) DEFAULT NULL,
  `localisation` varchar(100) DEFAULT NULL,
  `nb_segments` tinyint(4) NOT NULL DEFAULT '0',
  `numeration` enum('simple','MdF') DEFAULT 'simple',
  `iddomdef_m` tinyint(4) DEFAULT NULL,
  `iddomdef_d` tinyint(4) DEFAULT NULL,
  `iddomdef_l` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idetablissement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#================================================================ Etatfiches ===
DROP TABLE IF EXISTS Etatfiches;
CREATE TABLE `Etatfiches` (
  `idetatfiche` tinyint(4) NOT NULL AUTO_INCREMENT,
  `etatfiche` char(50) DEFAULT NULL,
  PRIMARY KEY (`idetatfiche`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO Etatfiches (idetatfiche, etatfiche) VALUES
  ('1','...'),
  ('2','*** 2. Transféré ***'),
  ('3','*** 3. Transfert refusé ***'),
  ('4','*** 4. Transfert en cours ***'),
  ('5','*** 5. Demande de transfert ***'),
  ('6','.6- Fiche transférable'),
  ('7','.7- Photos et textes en place- À valider'),
  ('8','.8- Photos en place'),
  ('9','.9- Textes en place'),
  ('10','10. Fiche à traiter en mode détaillé'),
  ('11','***11. Fiche validée, non transférable'),
  ('12','***12. Fiche partielle, non transférable'),
  ('13','***13. Fiche à supprimer (ou à réutiliser)'),
  ('14','***14. Fiche d&#039;identification simple'),
  ('15','***15. Fiche validée, objets introuvables');


#===================================================================== Etats ===
DROP TABLE IF EXISTS Etats;
CREATE TABLE `Etats` (
  `idetat` tinyint(4) NOT NULL AUTO_INCREMENT,
  `etat` char(25) DEFAULT NULL,
  `etat_en` char(25) DEFAULT NULL,
  PRIMARY KEY (`idetat`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO Etats (idetat, etat, etat_en) VALUES
  ('1','(non renseigné)','(not documented)'),
  ('2','état neuf, complet','new condition, complete'),
  ('3','bon, complet','good, complete'),
  ('4','bon, incomplet','good, incomplete'),
  ('5','bon, restauré','good, restored'),
  ('6','mauvais, à restaurer','bad, to restore'),
  ('7','mauvais, complet','bad, complete'),
  ('8','mauvais, incomplet','bad, incomplete'),
  ('9','moyen, complet','fair, complete'),
  ('10','moyen, incomplet','fair, incomplete');


#================================================================== Familles ===
DROP TABLE IF EXISTS Familles;
CREATE TABLE `Familles` (
  `idfamille` tinyint(4) NOT NULL AUTO_INCREMENT,
  `famille` char(20) DEFAULT NULL,
  `famille_en` char(20) DEFAULT NULL,
  `oriente` enum('oui','non') NOT NULL DEFAULT 'oui',
  PRIMARY KEY (`idfamille`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO Familles (idfamille, famille, famille_en, oriente) VALUES
  ('1','sous-ensemble','sub-assembly','oui'),
  ('2','documentation','documentation','oui'),
  ('3','logiciel','software','oui'),
  ('4','illustration','illustration','oui'),
  ('5','périphérique','peripheral','oui'),
  ('6','famille','same family','non');


#================================================================= Langprogs ===
DROP TABLE IF EXISTS Langprogs;
CREATE TABLE `Langprogs` (
  `idlangprog` tinyint(4) NOT NULL AUTO_INCREMENT,
  `langprog` char(20) DEFAULT NULL,
  `langprog_en` char(20) DEFAULT NULL,
  PRIMARY KEY (`idlangprog`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO Langprogs (idlangprog, langprog, langprog_en) VALUES
  ('1','(inconnu)','(unknown)'),
  ('2','Assembleur','Assembly code'),
  ('3','Fortran','Fortran'),
  ('4','Cobol','Cobol'),
  ('5','Lisp','Lisp'),
  ('6','APL','APL'),
  ('7','Algol','Algol'),
  ('8','PL1','PL1'),
  ('9','Basic','Basic'),
  ('10', 'Pascal', 'Pascal'),
  ('11','C','C'),
  ('12','C++','C++'),
  ('13','UNIX Shell','UNIX Shell'),
  ('14','Autre','Other');


#=================================================================== Langues ===
DROP TABLE IF EXISTS Langues;
CREATE TABLE `Langues` (
  `idlangue` tinyint(4) NOT NULL AUTO_INCREMENT,
  `langue` char(30) DEFAULT NULL,
  `langue_en` char(30) DEFAULT NULL,
  PRIMARY KEY (`idlangue`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO Langues (idlangue, langue, langue_en) VALUES
  ('1','français','French'),
  ('2','allemand','German'),
  ('3','anglais','English'),
  ('4','espagnol','Spanish'),
  ('5','néerlandais','Dutch'),
  ('6','portugais','Portugese'),
  ('7','russe','Russian'),
  ('8','français/anglais','French/English'),
  ('9','français/anglais/allemand','French/English/German'),
  ('10','langues européennes','European languages');


#===================================================================== Liens ===
DROP TABLE IF EXISTS Liens;
CREATE TABLE `Liens` (
  `idlien` mediumint(9) NOT NULL AUTO_INCREMENT,
  `idcol1` mediumint(9) NOT NULL DEFAULT '0',
  `idcol2` mediumint(9) NOT NULL DEFAULT '0',
  `commentlien` varchar(50) DEFAULT NULL,
  `idfamille` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idlien`),
  KEY `idcol1` (`idcol1`),
  KEY `idcol2` (`idcol2`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#================================================================= Logiciels ===
DROP TABLE IF EXISTS Logiciels;
CREATE TABLE `Logiciels` (
  `idlogiciel` mediumint(9) NOT NULL AUTO_INCREMENT,
  `titrelog` varchar(100) DEFAULT NULL,
  `titrelog_fr` varchar(100) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `idsysteme` tinyint(4) NOT NULL DEFAULT '1',
  `idcodeprog` tinyint(4) NOT NULL DEFAULT '1',
  `idlangprog` tinyint(4) NOT NULL DEFAULT '1',
  `idsupport` tinyint(4) NOT NULL DEFAULT '1',
  `idlangue` tinyint(4) NOT NULL DEFAULT '1',
  `res1l` varchar(100) DEFAULT NULL,
  `res2l` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idlogiciel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



#================================================================== Machines ===
DROP TABLE IF EXISTS Machines;
CREATE TABLE `Machines` (
  `idmachine` mediumint(9) NOT NULL AUTO_INCREMENT,
  `nomsecond` varchar(50) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `nom_en` varchar(50) DEFAULT NULL,
  `nomsecond_en` varchar(50) DEFAULT NULL,
  `modele` varchar(50) DEFAULT NULL,
  `nrserie` varchar(20) DEFAULT NULL,
  `descrimach` text,
  `alim` varchar(50) DEFAULT NULL,
  `puissance` varchar(50) DEFAULT NULL,
  `iddesignation` smallint(6) NOT NULL DEFAULT '1',
  `idservice` tinyint(4) NOT NULL DEFAULT '1',
  `res1m` varchar(100) DEFAULT NULL,
  `res2m` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idmachine`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



#================================================================= Materiaux ===
DROP TABLE IF EXISTS Materiaux;
CREATE TABLE `Materiaux` (
  `idmateriau` smallint(4) NOT NULL AUTO_INCREMENT,
  `materiau` varchar(80) DEFAULT NULL,
  `materiau_en` varchar(80) DEFAULT NULL,
  `materiauPSTC` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idmateriau`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#==================================================================== Medias ===
DROP TABLE IF EXISTS Medias;
CREATE TABLE `Medias` (
  `idmedia` mediumint(9) NOT NULL AUTO_INCREMENT,
  `descrimedia` varchar(100) DEFAULT NULL,
  `cotemedia` varchar(50) DEFAULT NULL,
  `lieumedia` varchar(30) DEFAULT NULL,
  `dimmedia` varchar(20) DEFAULT NULL,
  `pubreserv` enum('oui','non') NOT NULL DEFAULT 'non',
  `formatmedia` varchar(4) NOT NULL DEFAULT 'jpg',
  `dureemedia` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idmedia`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#================================================================== Motscles ===
DROP TABLE IF EXISTS Motscles;
CREATE TABLE `Motscles` (
  `idmotcle` mediumint(4) NOT NULL AUTO_INCREMENT,
  `motcle` varchar(50) DEFAULT NULL,
  `motcle_en` varchar(50) DEFAULT NULL,
  `cleaconit` enum('oui','non') NOT NULL DEFAULT 'non',
  PRIMARY KEY (`idmotcle`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#================================================================ Mouvements ===
DROP TABLE IF EXISTS Mouvements;
CREATE TABLE `Mouvements` (
  `idmouvement` mediumint(9) NOT NULL AUTO_INCREMENT,
  `datemouv` date DEFAULT NULL,
  `idtypemouvt` tinyint(4) NOT NULL DEFAULT '1',
  `orgmouv` varchar(50) DEFAULT NULL,
  `destmouv` varchar(50) DEFAULT NULL,
  `comgenmouv` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idmouvement`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



#================================================================ Organismes ===
DROP TABLE IF EXISTS Organismes;
CREATE TABLE `Organismes` (
  `idorganisme` mediumint(9) NOT NULL AUTO_INCREMENT,
  `osigle` varchar(12) DEFAULT NULL,
  `f_secable` tinyint(4) NOT NULL DEFAULT '0',
  `onom` char(100) DEFAULT NULL,
  `onotes` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idorganisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#================================================================ Parametres ===
DROP TABLE IF EXISTS Parametres;
CREATE TABLE `Parametres` (
  `idparametre` tinyint(4) NOT NULL AUTO_INCREMENT,
  `parametre` varchar(50) DEFAULT NULL,
  `valeur` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idparametre`),
  KEY `idparametre` (`idparametre`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO Parametres (idparametre, parametre, valeur) VALUES
  ('1','Base','v26'),
  ('2','FR_msg',NULL),
  ('3','EN_msg',NULL),
  ('4','etat_public','2, 4, 5, 6, 11, 12'),
  ('5','taille_bloc','500'),
  ('6','titre_avec_designation','0'),
  ('7','titre_avec_type','1'),
  ('8','affichage_proprietaire','1'),
  ('9','type_objet','1'),
  ('10','type_iconographie','1'),
  ('11','code_barre','0'),
  ('12','mode_recolement','0'),
  ('13','bloquer_desherbage','1'),
  ('14','poids_trecols','0,30,25,20,20,30,20,30,-20,-20,-20'),
  ('15','poids_tmarques','3,2,1,1,1,1,1,0,0,0');


#================================================================== Periodes ===
DROP TABLE IF EXISTS Periodes;
CREATE TABLE `Periodes` (
  `idperiode` tinyint(4) NOT NULL AUTO_INCREMENT,
  `periode` char(20) DEFAULT NULL,
  PRIMARY KEY (`idperiode`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO Periodes (idperiode, periode) VALUES
  ('1','-'),
  ('2','1750-1775'),
  ('3','1775-1800'),
  ('4','1800-1825'),
  ('5','1825-1850'),
  ('6','1850-1875'),
  ('7','1875-1900'),
  ('8','1900-1925'),
  ('9','1925-1950'),
  ('10','1950-1975'),
  ('11','1975-2000'),
  ('12','2000-2025'),
  ('13','2025-2050'),
  ('14','(inconnu)');


#================================================================= Personnes ===
DROP TABLE IF EXISTS Personnes;
CREATE TABLE `Personnes` (
  `idpersonne` mediumint(9) NOT NULL AUTO_INCREMENT,
  `pcivil` char(4)  DEFAULT NULL,
  `pprenom` char(40) DEFAULT NULL,
  `pnom` char(40) DEFAULT NULL,
  `pnotes` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idpersonne`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#=============================================================== Productions ===
DROP TABLE IF EXISTS Productions;
CREATE TABLE `Productions` (
  `idproduction` tinyint(4) NOT NULL AUTO_INCREMENT,
  `production` char(20) DEFAULT NULL,
  `production_en` char(20) DEFAULT NULL,
  PRIMARY KEY (`idproduction`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO Productions (idproduction, production, production_en) VALUES
  ('1','(non renseigné)','(not documented)'),
  ('2','(inconnu)','(unknown)'),
  ('3','grande série','mass production'),
  ('4','petite série','small production'),
  ('5','prototype','prototype'),
  ('6','unique','unique'),
  ('7','moyenne série','medium series');


#=============================================================== Rapporteurs ===
DROP TABLE IF EXISTS Rapporteurs;
CREATE TABLE `Rapporteurs` (
  `idrapporteur` smallint(6) NOT NULL AUTO_INCREMENT,
  `rprenom` varchar(20) DEFAULT NULL,
  `rnom` varchar(20) DEFAULT NULL,
  `rmel` varchar(50) DEFAULT NULL,
  `statut` enum('senior_admin','administrateur','rapporteur','expert','client_web') NOT NULL DEFAULT 'expert',
  `login` varchar(20) DEFAULT NULL,
  `passe` char(41) NOT NULL,
  `indice` varchar(50) DEFAULT NULL,
  `droitlocal` varchar(20) DEFAULT NULL,
  `droitperso` varchar(30) DEFAULT NULL,
  `profilh` char(12) DEFAULT NULL,
  PRIMARY KEY (`idrapporteur`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO Rapporteurs (idrapporteur, rprenom, rnom, rmel, statut, login, passe, indice, droitlocal, droitperso, profilh) VALUES
  ('1','','ROOT','info@aconit.org','senior_admin','root','*9D48286A08DD3E33B54B9527CFE080A171D16346','Ampère',NULL,NULL,'f8c07de03000'),
('2','Philippe','Denoyelle','pdenoyelle@mac.com','senior_admin','pdenoyelle','*639586D16A4C9F9D2455A8732B788010ED06FDBF','pique',NULL,'sup_objet','f8c07de03000');


#================================================================= Relations ===
DROP TABLE IF EXISTS Relations;
CREATE TABLE `Relations` (
  `idrelation` tinyint(4) NOT NULL AUTO_INCREMENT,
  `relation` char(30) DEFAULT NULL,
  `relation_en` char(30) DEFAULT NULL,
  `clepersonne` enum('oui','non') NOT NULL DEFAULT 'oui',
  `cleorganisme` enum('oui','non') NOT NULL DEFAULT 'oui',
  PRIMARY KEY (`idrelation`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO Relations (idrelation, relation, relation_en, clepersonne, cleorganisme) VALUES
  ('1','Auteur','Author','oui','oui'),
  ('2','Correspondant collection','collection expert','oui','non'),
  ('3','Crédits photos','Photos credit','oui','oui'),
  ('4','Donateur','Donor','oui','oui'),
  ('5','Editeur','Editor','non','oui'),
  ('6','Fabricant','Manufacturer','non','oui'),
  ('7','Dépositaire','Depository','non','non'),
  ('8','Distributeur','Distributor','non','oui'),
  ('9','Inventeur','Inventor','oui','non'),
  ('10','Propriétaire','Owner','oui','oui'),
  ('11','Responsable inventaire','Inventory responsible','oui','non'),
  ('12','Utilisateur','User','oui','oui'),
  ('13','Marque','Label','non','oui'),
  ('14','Collecteur','Collector','oui','non');


#=============================================================== SQLrequetes ===
DROP TABLE IF EXISTS SQLrequetes;
CREATE TABLE `SQLrequetes` (
  `idsqlrequete` smallint(4) NOT NULL AUTO_INCREMENT,
  `idrapporteur` smallint(4) DEFAULT NULL,
  `nom` varchar(40) DEFAULT NULL,
  `requete` text DEFAULT NULL,
  PRIMARY KEY (`idsqlrequete`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO SQLrequetes (idsqlrequete, idrapporteur, nom, requete) VALUES
  ('1','-1','Derniers 100 modifiés','Collections.idcollection > 0 ; ORDER BY datemod DESC LIMIT 100'),
  ('2','-1','Derniers 100 créés','Collections.idcollection > 0 ; ORDER BY datecreat DESC LIMIT 100'),
  ('3','0','Mouvements','mouvement LIKE \'%Minatec%\''),
  ('4','0','Impression liste','Collections.idcollection &gt; 0 AND Collections.idcollection &lt;= 1000; ORDER BY Collections.idcollection ASC');
 

#==================================================================== Series ===
DROP TABLE IF EXISTS Series;
CREATE TABLE `Series` (
  `idserie` smallint(9) NOT NULL AUTO_INCREMENT,
  `idetablissement` smallint(6) DEFAULT NULL,
  `codeserie` char(12) DEFAULT NULL,
  `comserie` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idserie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



#================================================================== Services ===
DROP TABLE IF EXISTS Services;
CREATE TABLE `Services` (
  `idservice` tinyint(4) NOT NULL AUTO_INCREMENT,
  `service` char(20) DEFAULT NULL,
  `service_en` char(20) DEFAULT NULL,
  PRIMARY KEY (`idservice`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO Services (idservice, service, service_en) VALUES
  ('1','(inconnu)','(unknown)'),
  ('2','hors service','out of service'),
  ('3','service partiel','partial service'),
  ('4','opérationnel','operational');


#================================================================== Supports ===
DROP TABLE IF EXISTS Supports;
CREATE TABLE `Supports` (
  `idsupport` tinyint(4) NOT NULL AUTO_INCREMENT,
  `support` char(30) DEFAULT NULL,
  `support_en` char(30) DEFAULT NULL,
  PRIMARY KEY (`idsupport`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

INSERT INTO Supports (idsupport, support, support_en) VALUES
  ('1','(non renseigné)','(not documented)'),
  ('2','listing papier','printout'),
  ('3','carte perforées','punched cards'),
  ('4','ruban perforé','punched tape'),
  ('5','bande magnétique 1/2 pouce','Magnetic tape 1/2&quot;'),
  ('6','bande magnétique divers','Miscelleaneous magnetic tape'),
  ('7','disquette 8 pouces','floppy disk 8&quot;'),
  ('8','disquette 5,25 pouces','floppy disk 5&quot;1/4'),
  ('9','disquette 3,5 pouces','floppy disk 3.5&quot;'),
  ('10','CD','CD'),
  ('11','DVD','DVD'),
  ('12','cartouche magnétique','magnetic cartridge'),
  ('13','cassette','cassette'),
  ('15','PROM','PROM'),
  ('16','Cartouche optique','optical cartridge'),
  ('17','Cartouche mémoire','Memory cartridge'),
  ('18','Cassettes DECTAPE II','DECTAPE II cartridge'),
  ('19','Disquette Iomega 100','Iomega100 disk'),
  ('20','autre support','other media');


#================================================================== Systemes ===
DROP TABLE IF EXISTS Systemes;
CREATE TABLE `Systemes` (
  `idsysteme` tinyint(4) NOT NULL AUTO_INCREMENT,
  `systeme` char(20) DEFAULT NULL,
  PRIMARY KEY (`idsysteme`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

INSERT INTO Systemes (idsysteme, systeme) VALUES
  ('1','inconnu'),
  ('2','Autre'),
  ('3','IBM OS'),
  ('4','VAX VMS'),
  ('5','Unix'),
  ('6','Linux'),
  ('7','MS/DOS'),
  ('8','Windows'),
  ('9','Mac OS 1 à 9'),
  ('10','Mac OS X'),
  ('11','Windows et Mac'),
  ('12','Amstrad'),
  ('13','Atari'),
  ('14','Thomson'),
  ('15','Amiga'),
  ('16','TRSDOS'),
  ('17','FLEX'),
  ('18','Apple DOS'),
  ('19','PDP-8 ou 11'),
  ('20','Philips P880');


#================================================================== Tmarques ===
DROP TABLE IF EXISTS Tmarques;
CREATE TABLE `Tmarques` (
  `idtmarque` tinyint(4) NOT NULL AUTO_INCREMENT,
  `tmarque` char(20) DEFAULT NULL,
  PRIMARY KEY (`idtmarque`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO Tmarques (idtmarque, tmarque) VALUES
  ('1','1-rareté'),
  ('2','2-état'),
  ('3','3-contenu'),
  ('4','4-provenance'),
  ('5','5-réparable'),
  ('6','6-exposable'),
  ('7','7-doublure'),
  ('8','8-objet lié'),
  ('9','9-composant'),
  ('10','VERROU (validé)');


#=============================================================== Tnpositions ===
DROP TABLE IF EXISTS Tnpositions;
CREATE TABLE `Tnpositions` (
  `idtnposition` tinyint(4) NOT NULL AUTO_INCREMENT,
  `tnposition` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtnposition`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



#=================================================================== Trecols ===
DROP TABLE IF EXISTS Trecols;
CREATE TABLE `Trecols` (
  `idtrecol` tinyint(4) NOT NULL AUTO_INCREMENT,
  `trecol` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtrecol`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO Trecols (idtrecol, trecol) VALUES
  ('1','1-  non défini'),
  ('2','2- informatique grenobloise'),
  ('3','3- référentiel France'),
  ('4','4- représentativité internationale'),
  ('5','5- matériau pédagogique'),
  ('6','6- document rare ou ancien'),
  ('7','7- document technique ou de recherche'),
  ('8','8- logiciel exploitable'),
  ('9','A- machine indifférenciée ou à jeter'),
  ('10','B- document non caractérisé ou à jeter'),
  ('11','C- logiciel non exploitable');


#================================================================== Typedocs ===
DROP TABLE IF EXISTS Typedocs;
CREATE TABLE `Typedocs` (
  `idtypedoc` tinyint(4) NOT NULL AUTO_INCREMENT,
  `typedoc` char(30) DEFAULT NULL,
  `typedoc_en` char(30) DEFAULT NULL,
  PRIMARY KEY (`idtypedoc`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

INSERT INTO Typedocs (idtypedoc, typedoc, typedoc_en) VALUES
  ('1','(non renseigné)','(not documented)'),
  ('2','Livre','Book'),
  ('3','Notice','Manual'),
  ('4','Brevet','Patent'),
  ('5','Journal','Newspaper'),
  ('6','Publicité','Advertisement'),
  ('7','Documentation commerciale','Commercial documentation'),
  ('8','Facture','Invoice'),
  ('9','Manuscrit','Handwritten document'),
  ('10','Emballage','Packaging'),
  ('11','Manuel','Manual'),
  ('12','Revue','Magazine'),
  ('13','Commande','Sales order'),
  ('14','Cahier de laboratoire','Laboratory notebook'),
  ('15','Catalogue','Catalog'),
  ('16','Actes colloque','Conference proceedings'),
  ('17','Photographie','Photograph'),
  ('18','Plan','Drawing'),
  ('19','Panneau d&#039;exposition','Exhibition panel'),
  ('20','Thèse','Thesis'),
  ('21','Classeur','Binder'),
  ('22','Mallette de rangement','Flight case'),
  ('23','Fiche technique (Datasheet)','Datasheet'),
  ('24','Documentation juridique','Legal documentation'),
  ('25','Guide','Guide'),
  ('26','Mode d&#039;emploi','Instructions for use'),
  ('27','Schéma','Diagram'),
  ('28','Copie','Duplicate'),
  ('29','Documentation technique','Technical documentation'),
  ('30','Manuel de référence','Reference manual'),
  ('31','Cours','Lesson notes'),
  ('32','Mémoire','Report'),
  ('33','Note technique','Technical note'),
  ('34','Ensemble','Set');


#============================================================== Typeillustrs ===
DROP TABLE IF EXISTS Typeillustrs;
CREATE TABLE `Typeillustrs` (
  `idtypeillustr` tinyint(4) NOT NULL AUTO_INCREMENT,
  `typeillustr` char(30) DEFAULT NULL,
  `typeillustr_en` char(30) DEFAULT NULL,
  PRIMARY KEY (`idtypeillustr`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO Typeillustrs (idtypeillustr, typeillustr, typeillustr_en) VALUES
  ('1','(non rensigné)','(not documented)'),
  ('2','aucune','none'),
  ('3','dessins','drawings'),
  ('4','gravures','engravings'),
  ('5','photos N&B','B&W photographs'),
  ('6','photos couleur','color potographs'),
  ('7','photos N&B, dessins','B&W photos, drawings'),
  ('8','photos et dessins','photos and drawings'),
  ('9','schémas','diagrams');


#================================================================ Typemouvts ===
DROP TABLE IF EXISTS Typemouvts;
CREATE TABLE `Typemouvts` (
  `idtypemouvt` tinyint(4) NOT NULL AUTO_INCREMENT,
  `typemouvt` char(30) DEFAULT NULL,
  `typemouvt_en` char(30) DEFAULT NULL,
  PRIMARY KEY (`idtypemouvt`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO Typemouvts (idtypemouvt, typemouvt, typemouvt_en) VALUES
  ('1','prêt','loan'),
  ('2','dépot','permanent loan'),
  ('3','retour','return'),
  ('4','réparation','repair works'),
  ('5','exposition','exhibition');


#=================================================================== Verrous ===
DROP TABLE IF EXISTS Verrous;
CREATE TABLE `Verrous` (
  `idrapporteur` smallint(6) DEFAULT NULL,
  `idcollection` mediumint(9) DEFAULT NULL,
  `dateverrou` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `idcollection` (`idcollection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


