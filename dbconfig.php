<?php
######### Modèle de fichier config.php à adapter suivant la configuration (une ou plusieurs bases...) #############
######### Prévu ici pour une plateforme de test : adapter $debug et error_reporting pour production   #############

	$debug = 0x80; 						# 1:DebugMSG, 2: Aff liens, Import, 4:Evalstring, 8:Xpose
														# 0x10:$_SESSION, 0x20:$_REQUEST, 0x40:$INPUT,
														# 0x80:error backtrace
																		
	$maintenance = 0;													# Bloque l'accès des utilisateurs à la BdD
  error_reporting (-1);											# Toutes erreurs pour serveur de test local

	$dir_img			= '../dbimg/';								# Dossier des images spécifiques aux bases
	$dir_export		= '../dbexport/';							# Dossier export des fichiers de transfert
	$dir_plugin		= '../dbplugin/';							# Dossier des fichiers externes ajoutés à DBAconit
	$dir_save			= '../dbsave/';								# Dossier des fichiers de sauvegarde
	$dir_tempo		= '../dbtempo/';							# Dossier (temporaire) des fichiers temporaires
 
  $dir_media		= '../dbmedia';								# Base du nom des dossiers medias
	$dba_retour		= '../../www/';								# Adresse de retour vers site web	
  
	$dir_dbgalerie	= '../dbgalerie/';					# Dossier galerie virtuelle (s'il existe...)

	$dba_comptage		= TRUE;											# Comptage des login, visiteurs... conflits
	$dba_sauv_auto	= FALSE;										# Autorise la sauvegarde automatique
	
									# Choix des tables affichées par bloc (les listes sont notées : list_org et list_per)
	$dba_affbloc	= array ('Medias', 'Organismes', 'Personnes', 'list_org', 'list_per');
	
	$url_galerie	= '../dbgalerie/galerie.php?fgal=galerie'; 			# Base de l'URL des salles galerie	
	$url_media		= 'https://??????????????????/db/dbmedia';			# Base du nom des dossiers médias

	$Bases = array (

		array (												#### Base 0
		'serveur'	=> '127.0.0.1', 										# Nom du serveur MySQL
		'user' 		=> 'root', 													# Nom de l'utilisateur enregistré dans MySQL
		'passe'		=> '?????????', 										# Mot de passe du serveur MySQL  
		'nom'			=> 'aconit_loc',										# Nom de la base MySQL
		
		'nompublic'	=> 'ACONIT_informatique', 				# Nom public de la base
		'icon'		=> $dir_img.'l_aconit.png',					#	Logo ajouté sur la tétière (h=48 imposé)
		'portail'	=> $dir_img.'p_aconit.png',					#	Image d'accueil page identification (~ 100x100)
		'tetiere'	=> $dir_img.'f_aconit.jpg'					# Tétière des pages HTML (1OOO x 48 imposé)
		'nbr_cars_nrinv' => 4													# Nombre de caractères imposé au No inventaire imposé : 0001...
		),
	
		array (												##### Base 1		# Même format pour les bases successives 1, 2...
		),
		
	);
	
/*
Copyright ou © ou Copr. Association ACONIT
	contributeur : Philippe Denoyelle, (2002-2023) 
	pdenoyelle@aconit.org - info@aconit.org

Ce logiciel est un programme informatique servant à gérer une base de 
données inventaire du patrimoine, en ligne sous MySQL. 
Il s'applique également au logiciel DBAconit et à son annexe DBgalerie

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/
?>