## Installation de DBAconit

## Licence CeCILL
Rappel : DBAconit est diffusé sous licence CeCILL

## Prérequis
DBAconit peut être porté sur tout serveur Internet supportant PHP et MySQL. Il est accessible à partir de toute machine ayant accès au réseau, à partir d'un navigateur internet standard, compatible HTML5.

DBAconit est actuellement testé sous :
- PHP v7.2   (avec utilisation de la bibliothèque GDImage)
- MySQL v8.0 (mais toujours compatible avec v5.5)
- CSS 3

### Medias

Pour que le téléversement de medias (images) fonctionne il faut activer le module [GD](https://www.php.net/manual/en/book.image.php).
Si vous installez PHP en utilisant votre gestionnaire de paquets, suivez les instructions de ce-dernier. Par exemple sous debian :

```
apt install php-gd
```

Si vous compilez PHP, utilisez les arguments suivants pour `configure` (à ajuster selon votre cas) :

```
configure --with-gd --with-jpeg-dir=/usr/include --with-png-dir=/usr/include ...
```

### MySQL

Afin de le code dbaconit puisse interagir avec la base de données, il faut activer le module [mysqli](https://www.php.net/manual/en/book.mysqli.php).
Si vous installez PHP en utilisant votre gestionnaire de paquets, suivez les instructions de ce-dernier. Par exemple sous debian :

```
apt install php-mysql php-db
```

Si vous compilez PHP, utilisez les arguments suivants pour `configure` (à ajuster selon votre cas) :

```
configure --with-mysql --with-mysqli ...
```

### Docker

Voici un exemple de `Dockerfile` pour la partie PHP qui se charge notamment des dépendances `mysqli` et `gd` :

```
ARG PHP_VERSION="7.2"
FROM php:${PHP_VERSION:+${PHP_VERSION}-}fpm-alpine

RUN apk update; \
    apk add libpng-dev libjpeg-turbo-dev; \
    apk upgrade;

RUN docker-php-ext-configure gd --with-jpeg-dir=/usr/include --with-png-dir=/usr/include
RUN docker-php-ext-install mysqli gd

COPY php.ini /usr/local/etc/php/conf.d/php.ini
```

## Documentation
L'ensemble de la documentation se trouve dans le dossier [Documentation](https://db.aconit.org/documentation/),
en particulier :
1. Notice d'utilisation DBAconit
2. Structure dDBAconit
3. Notice technique DBAconit
	voir la notice d'installation au paragraphe 18 (extrait ci-dessous)
	
__________________

## 18- Installation DBAconit 
La première version de DBAconit était accompagnée d’un programme d’installation. Ce programme, peu utilisé était difficile à tenir à jour et a été abandonné. La façon la plus simple de créer une nouvelle configuration de DBAconit est d’utiliser une copie nettoyée d’une base existante.

### 18.1- Mise en place de la structure des dossiers 
Créer le dossier de base, généralement db/.  
Dans db/ 
- Ouvrir  le dépôt [gitlab dbaconit](https://gitlab.com/aconit_grenoble/db/dbaconit)
- copier le dossier programme dbaconit/ complet
- créer les dossiers vides dbexport, dbsave.  
	dbtempo est créé automatiquement si nécessaire
- créer le dossier dbmedia_0/ qui recevra de façon automatique tous sous-dossiers jpg0, pdf0...

- Ouvrir le dépôt [gitlab dbinstal](https://gitlab.com/aconit_grenoble/db/dbinstal)
- copier le fichier dbconfig.php, ajuster la configuration.
- copier le dossier dbimg/, ajuster les images suivant les besoins

### 18.2- Création de la base MySQL et chargement initial
- Créer une base vide avec PHPmyAdmin. 
	- Noter le nom du serveur, de la base et le mot de passe…		
	- Reporter ses valeurs dans le fichier db/dbconfig.php/
- Prendre le fichier DBAconit_v26.1_base_vide.sql (dans le dépôt dbinstal ouvert ci-dessus)
- Avec PHPmyAdmin, importer ce fichier de sauvegarde dans votre nouvelle base.  
*Utilisez l'utilisateur 'root'  et le mot de passe 'ampere'.** Il vous faudra ensuite le modifier rapidement ! (dans la table Rapporteurs')

On conseille de bien vérifier toute l’organisation des tables d’ordre avant de commencer à charger des objets : tant qu’aucun lien n’est établi !
____________________
